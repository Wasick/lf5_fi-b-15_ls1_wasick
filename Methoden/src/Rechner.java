
public class Rechner {

	public static void main(String[] args) {
		
		erklaerung();
		double wert1 = wert1();
		double wert2 = wert2();
		double ergebniss = berechnung(wert1, wert2);
		ausgabe(ergebniss);
	}
	
	public static void erklaerung() {
		System.out.println("Dieses Programm wird multiplizieren: ");
		System.out.println("-------------------------------------------------------");
	}
	
	public static double wert1() {
		double wert1 = 2.36;
		return wert1;
	}
	
	public static double wert2() {
		double wert2 = 7.87;
		return wert2;
	}
	
	public static double berechnung(double wert1, double wert2) {
		 double ergebniss = wert1 * wert2;
		 return ergebniss;
	}
	
	public static void ausgabe(double ergebniss) {
		System.out.printf("Ergebniss = %.2f", ergebniss);
	}
}
