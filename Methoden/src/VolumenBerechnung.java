import java.util.Scanner;

public class VolumenBerechnung {

	public static void main(String[] args) {
		
		erklaerung();
		double A = wertA();
		double B = wertB();
		double C = wertC();
		double H = wertH();
		double R = wertR();
		double pi = wertPi();
		
		double wuerfel = berechnungWuerfel(A);
		ausgabeWuerfel(wuerfel);
		
		double quader = berechnungQuader(A, B, C);
		ausgabeQuader(quader);
		
		double pyramide = berechnungPyramide(A, H);
		ausgabePyramide(pyramide);
		
		double kugel = berechnungKugel(R, pi);
		ausgabeKugel(kugel);
		
	}
	
	public static void erklaerung() {
		System.out.println("Dieses Programm wird K�rper berechnen");
		System.out.println("-------------------------------------");
	}
	
	public static double wertA() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie einen Wert fuer A an: ");
		double A = tastatur.nextDouble();
		return A;
	}
	
	public static double wertB() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie einen Wert fuer B an: ");
		double B = tastatur.nextDouble();
		return B;
	}
	
	public static double wertC() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie einen Wert fuer C an: ");
		double C = tastatur.nextDouble();
		return C;
	}
	
	public static double wertH() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie einen Wert fuer H an: ");
		double H = tastatur.nextDouble();
		return H;
	}
	
	public static double wertR() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte geben sie einen Wert f�r R an: ");
		double R = tastatur.nextDouble();
		return R;
	}
	
	public static double wertPi() {
	double pi = Math.PI;
	return pi;
	}
	
	public static double berechnungWuerfel(double A) {
	double wuerfel = A * A * A;
	return wuerfel;
	}
	
	public static void ausgabeWuerfel(double wuerfel) {
		System.out.printf("Wuerfel = %.2f \n", wuerfel);
	}
	
	public static double berechnungQuader(double A, double B, double C) {
		double quader = A * B * C;
		return quader;
	}
	
	public static void ausgabeQuader(double quader ) {
		System.out.printf("Quader = %.2f \n", quader );
	}
	
	public static double berechnungPyramide(double A, double H) {
		double pyramide = ((A * A) * H) / 3;
		return pyramide;
	}
	
	public static void ausgabePyramide(double pyramide) {
		System.out.printf("Pyramide = %.2f \n", pyramide);
	}
	
	public static double berechnungKugel(double R, double pi) {
		double kugel = 4.0/3.0 * pi * R * R * R;
		return kugel;
	}
	
	public static void ausgabeKugel(double kugel) {
		System.out.printf("Kugel = %.2f \n", kugel);
	}
}
