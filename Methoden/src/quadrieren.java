import java.util.Scanner;

public class quadrieren {
   
	public static void main(String[] args) {

		titel();
		double x = eingabe();
		double ergebnis = verarbeitung(x);
		ausgabe(x, ergebnis);
	}
	
	public static double verarbeitung(double x) {
		double ergebnis= x * x;
		return ergebnis;
	}
	
	public static double eingabe() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte eine Zahl eingeben: ");
		double x = tastatur.nextDouble();
		return x;
	}
	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}
	
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
}

